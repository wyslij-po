/* This file is part of Wyslij-po
   Copyright (C) 2007-2021 Sergey Poznyakoff

   Wyslij-po is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Wyslij-po is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Wyslij-po.  If not, see <http://www.gnu.org/licenses/>. */

#include "wyslij-po.h"

static char prog_doc[] = N_("Submit PO files to the Translation Project");
static char args_doc[] = N_("FILE ...");

int dry_run;                    /* Do nothing, print everything */         
int verbose;                    /* Verbosity level */
char *langtab = "~/.wyslij-po.lc"; /* Name of user-specific file with language
				      names and abbreviations */
char *compress = "gzip -c";     /* Command to be used for compression */
char *address = "robot@translationproject.org"; /* E-mail of the TP robot */
char *subject_prefix = "TP-Robot "; /* Always prefix subject with this
				       string */
char const *sender;             /* Sender e-mail address */    
int set_sender_from_po;         /* If true, set sender e-mail from the PO
				   header (Last-Translator) */
char *cc;                       /* CC recipients */ 
char *bcc;                      /* BCC recipients */
char *fcc;                      /* FCC folder name */

/* What to check before submitting the PO file */
int verify_mask = VERIFY_PO_TIME | VERIFY_PACKAGE_VERSION;

#define SET_VERIFY_BIT(b) (verify_mask |= (b))
#define CLEAR_VERIFY_BIT(b) (verify_mask &= ~(b))
			   
/* Actual URL of the page of the domain %s on the TP */
char *tp_url = "http://translationproject.org/domain/${domain}.html";
/* Use this regex to guess the latest POT file name */
char *pot_regex_str =
  "The current template for this domain is <a href=\".[^\"]*/(.[^\"]*)\">";

mu_mailer_t mailer;

static void
opt_from (struct mu_parseopt *po, struct mu_option *opt, char const *arg)
{
  if (arg == NULL)
    set_sender_from_po = 1;
  else
    sender = arg;
}

static void
opt_verify_po_time (struct mu_parseopt *po, struct mu_option *opt,
		    char const *arg)
{
  if (strcmp (arg, "1") == 0)
    SET_VERIFY_BIT (VERIFY_PO_TIME);
  else
    CLEAR_VERIFY_BIT (VERIFY_PO_TIME);
}

static void
opt_verify_version (struct mu_parseopt *po, struct mu_option *opt,
		    char const *arg)
{
  if (strcmp (arg, "1") == 0)
    SET_VERIFY_BIT (VERIFY_PACKAGE_VERSION);
  else
    CLEAR_VERIFY_BIT (VERIFY_PACKAGE_VERSION);
}

static struct mu_option wy_options[] = {
  MU_OPTION_GROUP (N_("General Options:")),
  { "dry-run", 'n', NULL, MU_OPTION_DEFAULT,
    N_("do nothing, print what would have been done"),
    mu_c_bool, &dry_run },
  { "verbose", 'v', NULL, MU_OPTION_DEFAULT,
    N_("produce verbose output"),
    mu_c_incr, &verbose },
  { "langtab", 0, N_("FILE"), MU_OPTION_DEFAULT,
    N_("set the name of langtab file"),
    mu_c_string, &langtab },
  { "compress", 0, N_("COMMAND"), MU_OPTION_DEFAULT,
    N_("use this COMMAND for compression"),
    mu_c_string, &compress },
  { "gzip", 0, NULL, MU_OPTION_ALIAS },

  MU_OPTION_GROUP (N_("Mail Address Control:")),
  { "to", 't', N_("EMAIL"), MU_OPTION_DEFAULT,
    N_("send to this email address"),
    mu_c_string, &address },
  { "cc", 0, N_("EMAIL-LIST"), MU_OPTION_DEFAULT,
    N_("set Carbon-copy recipients"),
    mu_c_string, &cc },
  { "bcc", 0, N_("EMAIL-LIST"), MU_OPTION_DEFAULT,
    N_("set Blind carbon-copy recipients"),
    mu_c_string, &bcc },
  { "fcc", 0, N_("FOLDER"), MU_OPTION_DEFAULT,
    N_("direct Fcc to the given folder"),
    mu_c_string, &fcc },
  { "from", 'F', N_("EMAIL"), MU_OPTION_ARG_OPTIONAL,
    N_("set sender address"),
    mu_c_string, NULL, opt_from },

  MU_OPTION_GROUP (N_("PO File Verification Options:")),
  { "verify-po-time", 0, NULL, MU_OPTION_DEFAULT,
    N_("verify PO modification time"),
    mu_c_bool, NULL, opt_verify_po_time },
  { "verify-version", 0, NULL, MU_OPTION_DEFAULT,
    N_("make sure the submitted PO file is for the latest package version"),
    mu_c_bool, NULL, opt_verify_version },
#undef GRP
#if 0
  /* This entry is to pacify `make check-docs'.  The options below
     are defined in libmailutils.
  */
  { "mailer", },
  { "help", },
  { "usage", },
  { "version" },
#endif
  MU_OPTION_END
}, *options[] = { wy_options, NULL };

static char *capa[] = {
  "mailer",
  "debug",
  NULL
};

static int
_cb_verify_option (void *data, mu_config_value_t *arg, int bit)
{
  int flag;

  if (mu_cfg_assert_value_type (arg, MU_CFG_STRING))
    return 1;
  if (mu_str_to_c (arg->v.string, mu_c_bool, &flag, NULL))
    {
      mu_error (_("not a boolean"));
      return 1;
    }
  if (flag)
    SET_VERIFY_BIT (bit);
  else
    CLEAR_VERIFY_BIT (bit);
  return 0;
}

static int
_cb_verify_po_time (void *data, mu_config_value_t *arg)
{
  return _cb_verify_option (data, arg, VERIFY_PO_TIME);
}
    
static int
_cb_verify_version (void *data, mu_config_value_t *arg)
{
  return _cb_verify_option (data, arg, VERIFY_PACKAGE_VERSION);
}
    
struct mu_cfg_param wy_cfg_param[] = {
  { "compress", mu_c_string, &compress, 0, NULL,
    N_("Use <command> for compression."),
    N_("command") },
  { "langtab", mu_c_string, &langtab, 0, NULL,
    N_("Set the name of the langtab file."),
    N_("file") },
  { "to", mu_c_string, &address, 0, NULL,
    N_("Set recipient address. Multiple emails are allowed."),
    N_("email") },
  { "cc", mu_c_string, &cc, 0, NULL,
    N_("Set carbon copy recipients."),
    N_("emails") },
  { "bcc", mu_c_string, &bcc, 0, NULL,
    N_("Set blind carbon copy recipients."),
    N_("emails") },
  { "fcc", mu_c_string, &fcc, 0, NULL,
    N_("Save copy in <folder>."),
    N_("folder") },
  { "from", mu_c_string, &sender, 0, NULL,
    N_("Set sender address"),
    N_("email") },
  { "from-po", mu_c_bool, &set_sender_from_po, 0, NULL,
    N_("Read sender address from the PO file.") },
  { "verify-po-time", mu_cfg_callback, NULL, 0, _cb_verify_po_time,
    N_("Verify PO modification time"),
    N_("arg: bool") },
  { "verify-version", mu_cfg_callback, NULL, 0, _cb_verify_version,
    N_("Verify package version."),
    N_("arg: bool") },
  { "tp-url", mu_c_string, &tp_url, 0, NULL,
    N_("URL of textual domain page. "
       "Characters %s are replaced with the textual domain name. "
       "The default value is http://translationproject.org/domain/%s.html") },
  { "pot-regex", mu_c_string, &pot_regex_str, 0, NULL,
    N_("Regular expression to extract the latest POT file name from tp-url.") },
  { NULL }
};

static char *
concat (const char *a, const char *b, const char *c)
{
  size_t size = strlen (a) + strlen (b) + (c ? strlen (c) : 0 ) + 1;
  char *p = mu_alloc (size);
  strcpy (p, a);
  strcat (p, b);
  if (c)
    strcat (p, c);
  return p;
}

void
create_body (mu_message_t *pmsg, const char *name, const char *gzname)
{
  mu_message_t msg;
  int status;
  mu_body_t body;
  mu_header_t hdr;
  mu_stream_t bstream; /* Body stream */
  mu_stream_t cstream; /* Compressor stream */
  mu_stream_t estream; /* Encoder stream */
  char *cmdline;
  char *s;
  
  mu_message_create (pmsg, NULL);
  msg = *pmsg;

  mu_message_get_header (msg, &hdr);
  /* mu_headder_append (hdr, MU_HEADER_CONTENT_DESCRIPTION*/
  mu_header_append (hdr, MU_HEADER_CONTENT_TRANSFER_ENCODING, "base64");
  s = concat ("application/x-gzip; name=\"", gzname, "\"");
  mu_header_append (hdr, MU_HEADER_CONTENT_TYPE, s);
  free (s);

  mu_message_get_body (msg, &body);
  mu_body_get_streamref (body, &bstream);

  cmdline = concat (compress, " ", name);
  if (verbose > 1) 
    mu_printf (_("executing %s...\n"), cmdline);
  
  status = mu_command_stream_create (&cstream, cmdline, MU_STREAM_READ);
  if (status)
    {
      mu_error (_("%s: cannot create stream for `%s': %s"),
		name, cmdline, mu_strerror (status));
      exit (1);
    }
  
  status = mu_filter_create (&estream, cstream, "base64", MU_FILTER_ENCODE,
			     MU_STREAM_READ);
  mu_stream_unref (cstream);
  if (status)
    {
      mu_error (_("%s: cannot create base64 encoder stream: %s"),
		name, mu_strerror (status));
      exit (1);
    }

  status = mu_stream_copy (bstream, estream, 0, NULL);
  mu_stream_write (bstream, "\n", 1, NULL);
  mu_stream_unref (estream);
  mu_stream_unref (bstream);
  if (status)
    {
      mu_error (_("%s: cannot pipe to `%s': %s"),
		name, cmdline, mu_strerror (status));
      exit (1);
    }
}

void
wyslij_po (const char *name)
{
  mu_mime_t mime;
  mu_message_t msg;
  mu_header_t hdr;
  mu_stream_t stream;
  char *po_sender;
  char *poname = restore_file_name (name, &po_sender);
  char *s;
  
  if (verbose)
    mu_printf ("%s: %s\n", name, poname);
  
  mu_mime_create (&mime, NULL, 0);

  s = concat (poname, ".gz", NULL);
  create_body (&msg, name, s);
  free (s);
  mu_mime_add_part (mime, msg);
  mu_message_unref (msg);

  mu_mime_get_message (mime, &msg);
  mu_message_get_header (msg, &hdr);
  if (set_sender_from_po)
    mu_header_append (hdr, MU_HEADER_FROM, po_sender);
  else
    mu_header_append (hdr, MU_HEADER_FROM, sender);
  mu_header_append (hdr, MU_HEADER_TO, address);
  s = concat (subject_prefix, poname, NULL);
  if (cc)
    mu_header_append (hdr, MU_HEADER_CC, cc);
  if (bcc)
    mu_header_append (hdr, MU_HEADER_BCC, bcc);
  if (fcc)
    mu_header_append (hdr, MU_HEADER_FCC, fcc);
  mu_header_append (hdr, MU_HEADER_SUBJECT, s);
  free (s);
  mu_header_append (hdr, "X-Mailer", PACKAGE_STRING);

  if (verbose)
    {
      if (verbose > 3)
	{
	  mu_printf ("\n%s\n", _("Message:"));
	  mu_message_get_streamref (msg, &stream);
	}
      else
	{
	  mu_printf ("\n%s\n", _("Message headers:"));
	  mu_header_get_streamref (hdr, &stream);
	}

      mu_stream_copy (mu_strout, stream, 0, NULL);
      mu_stream_unref (stream);
    }

  if (dry_run)
    mu_printf (_("NOT submitting\n"));
  else
    {
      int status;
      if (verbose)
	mu_printf (_("Submitting %s\n"), name);
      if ((status = mu_mailer_send_message (mailer, msg, NULL, NULL)))
	{
	  mu_error (_("%s: Sending message failed: %s"),
		    name, mu_strerror (status));
	  exit (1);
	}
    }

  mu_mime_destroy (&mime);
  free (poname);
  free (po_sender);
}

static void
parse_langtabs (void)
{
  char *name;

  if (access (DEFAULT_LANGTAB_FILE, R_OK) == 0)
    parse_lang_datafile (DEFAULT_LANGTAB_FILE);

  name = mu_tilde_expansion (langtab, '/', NULL);
  
  if (access (name, R_OK) == 0)
    parse_lang_datafile (name);
}

void
open_mailer (void)
{
  int status;
  int mailer_flags = 0;
  mu_property_t property = NULL;

  if (dry_run)
    return;
  
  if ((status = mu_mailer_create (&mailer, NULL)))
    {
      const char *url = NULL;
      mu_mailer_get_url_default (&url);
      mu_error (_("Creating mailer `%s' failed: %s"),
		url, mu_strerror (status));
      exit (1);
    }
  if (verbose > 2)
    {
      mu_debug_level_t level;

      mu_debug_get_category_level (MU_DEBCAT_MAILER, &level);
      level |= MU_DEBUG_LEVEL_MASK (MU_DEBUG_TRACE0) |
	       MU_DEBUG_LEVEL_MASK (MU_DEBUG_PROT);
      if (verbose > 3)
	level |= MU_DEBUG_LEVEL_MASK (MU_DEBUG_TRACE7);
      mu_debug_set_category_level (MU_DEBCAT_MAILER, level);
    }
  
  mu_mailer_get_property (mailer, &property);
  mu_property_set_value (property, "READ_RECIPIENTS", "true", 1);
  
  if ((status = mu_mailer_open (mailer, mailer_flags)))
    {
      const char *url = NULL;
      mu_mailer_get_url_default (&url);
      mu_error (_("Opening mailer `%s' failed: %s"),
		url, mu_strerror (status));
      exit (1);
    }
}

void
close_mailer (void)
{
  int status;
  
  if (!mailer)
    return;
  
  if ((status = mu_mailer_close (mailer)))
    {
      mu_error (_("Closing mailer failed: %s"),
		mu_strerror (status));
      exit (1);
    }

  mu_mailer_destroy (&mailer);
}

static const char version_etc_copyright[] =
  /* Do *not* mark this string for translation.  %s is a copyright
     symbol suitable for this locale, and %d is the copyright
     year.  */
  "Copyright %s %d Sergey Poznyakoff";
static int copyright_year = 2021;

void
wy_version (struct mu_parseopt *po, mu_stream_t stream)
{
  mu_stream_printf (stream, "%s (%s) %s\n", po->po_prog_name,
		    PACKAGE, PACKAGE_VERSION);
  /* TRANSLATORS: Translate "(C)" to the copyright symbol
     (C-in-a-circle), if this symbol is available in the user's
     locale.  Otherwise, do not translate "(C)"; leave it as-is.  */
  mu_stream_printf (stream, version_etc_copyright, _("(C)"), copyright_year);
  mu_stream_printf (stream, "%s", _("\
\n\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
\n\
"));
  /* TRANSLATORS: %s denotes an author name.  */
  mu_stream_printf (stream, _("Written by %s.\n"), "Sergey Poznyakoff");
}

static struct mu_cli_setup cli = {
  .optv = options,
  .cfg = wy_cfg_param,
  .prog_doc = prog_doc,
  .prog_args = args_doc,
};

void
my_getopt(int *pargc, char ***pargv, char **capa)
{
  struct mu_parseopt pohint;
  struct mu_cfg_parse_hints cfhint;
  
  pohint.po_flags = 0;
	
  pohint.po_package_name = PACKAGE_NAME;
  pohint.po_flags |= MU_PARSEOPT_PACKAGE_NAME;
  
  pohint.po_package_url = PACKAGE_URL;
  pohint.po_flags |= MU_PARSEOPT_PACKAGE_URL;

  pohint.po_bug_address = PACKAGE_BUGREPORT;
  pohint.po_flags |= MU_PARSEOPT_BUG_ADDRESS;

  pohint.po_version_hook = wy_version;
  pohint.po_flags |= MU_PARSEOPT_VERSION_HOOK;
  
  pohint.po_negation = "no-";
  pohint.po_flags |= MU_PARSEOPT_NEGATION;

  cfhint.site_file = mu_site_config_file ();
  cfhint.flags = MU_CFHINT_SITE_FILE | MU_CFHINT_PER_USER_FILE;

  mu_cli_ext (*pargc, *pargv, &cli, &pohint, &cfhint, capa, NULL,
	      pargc, pargv);
}

int
main (int argc, char **argv)
{
  int i;

#ifdef ENABLE_NLS
  mu_init_nls ();
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);
#endif
  mu_register_all_mailer_formats ();
  my_getopt (&argc, &argv, capa);

  if (0 == argc)
    {
      mu_error (_("Not enough arguments"));
      exit (1);
    }

  if (dry_run)
    verbose++;
  
  if (!sender && !set_sender_from_po)
    sender = mu_get_user_email (NULL);
  
  parse_langtabs ();
  compile_pot_regex_str ();
  
  open_mailer ();
  
  for (i = 0; i < argc; i++)
    wyslij_po (argv[i]);

  close_mailer ();
  
  exit (0);
}
