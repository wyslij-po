/* This file is part of Wyslij-po
   Copyright (C) 2007-2021 Sergey Poznyakoff

   Wyslij-po is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Wyslij-po is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Wyslij-po.  If not, see <http://www.gnu.org/licenses/>. */

#include "wyslij-po.h"

static char rbuf[1024];

static size_t io_timeout = 3;
static size_t io_attempts = 3;

static regex_t *pot_regex;

static int
http_stream_wait (mu_stream_t stream, int flags, size_t *attempt)
{
  int rc;
  int oflags = flags;
  struct timeval tv;

  while (*attempt < io_attempts)
    {
      tv.tv_sec = io_timeout;
      tv.tv_usec = 0;
      rc = mu_stream_wait (stream, &oflags, &tv);
      switch (rc) {
      case 0:
	if (flags & oflags)
	  return 0;
	/* FALLTHROUGH */
      case EAGAIN:
      case EINPROGRESS:
	++*attempt;
	continue;
	
      default:
	return rc;
      }
    }
  return ETIMEDOUT;
}

static int
parse_project_url (mu_url_t url, char **phost, char **ppath, unsigned *pport)
{
  int status;
  char const *s;
  
  status = mu_url_sget_scheme (url, &s);
  if (status)
    {
      mu_error (_("cannot get protocol from the url `%s': %s"),
		mu_url_to_string (url), mu_strerror (status));
      return 1;
    }

  if (strcmp (s, "http"))
    {
      mu_error (_("%s: unsupported protocol"), s);
      return 1;
    }

  status = mu_url_aget_host (url, phost);
  if (status)
    {
      mu_error (_("cannot extract host information from the url `%s': %s"),
		mu_url_to_string (url), mu_strerror (status));
      return 1;
    }

  status = mu_url_aget_path (url, ppath);
  if (status)
    {
      free (*phost);
      //FIXME
      mu_error (_("cannot extract host information from the url `%s': %s"),
		mu_url_to_string (url), mu_strerror (status));
      return 1;
    }
      
  mu_url_get_port (url, pport);
    
  return 0;
}

static int
create_project_url (const char *project, 
		    char **phost, char **ppath, unsigned *pport)
{
  char *url_str = NULL;
  struct mu_wordsplit ws;
  mu_url_t url;
  int rc;
  const char *env[3] = { "domain", project, NULL };

  ws.ws_env = env;
  rc = mu_wordsplit (tp_url, &ws,
		     MU_WRDSF_NOCMD | MU_WRDSF_UNDEF |
		     MU_WRDSF_ENV | MU_WRDSF_ENV_KV | MU_WRDSF_NOSPLIT);
  if (rc)
    {
      mu_error (_("cannot expand string `%s': %s"), tp_url,
		mu_wordsplit_strerror (&ws));
      return rc;
    }
		    
  rc = mu_url_create (&url, ws.ws_wordv[0]);
  if (rc)
    {
      mu_error (_("Cannot create url from `%s': %s"),
		ws.ws_wordv[0], mu_strerror (rc));
      mu_wordsplit_free (&ws);
      return 1;
    }
  mu_wordsplit_free (&ws);
  rc = parse_project_url (url, phost, ppath, pport);
  mu_url_destroy (&url);
  free (url_str);
  return rc;
}

static int
http_send (mu_stream_t stream, char const *cmd)
{ 
  int ret, off = 0;
  size_t nb, size;
  size_t attempt;
 
  for (attempt = 0, size = strlen (cmd); size > 0; )
    {
      ret = mu_stream_write (stream, cmd + off, size, &nb);
      if (ret == 0)
	{
	  if (nb == 0)
	    {
	      mu_error (_("mu_stream_write: wrote 0 bytes"));
	      return 1;
	    }
	  off += nb;
	  size -= nb;
	}
      else if (ret == EAGAIN)
        {
	  if (attempt < io_attempts)
	    {
	      ret = http_stream_wait (stream, MU_STREAM_READY_WR, &attempt);
	      if (ret)
		{
		  mu_error (_("http_wait failed: %s"), mu_strerror (ret));
		  return 1;
		}
	      continue;
	    }
	  else
	    {
	      mu_error (_("mu_stream_write timed out"));
	      return 1;
	    }
	}
      else
	{
	  mu_error ("mu_stream_write: %s", mu_strerror (ret));
	  return 1;
	}
    }
  return 0;
}

static int
http_read (mu_stream_t stream, mu_opool_t pool)
{
  int ret;
  size_t nb;
  size_t attempt = 0;

  for (;;)
    {
      ret = mu_stream_read (stream, rbuf, sizeof (rbuf), &nb);
      if (ret == 0)
	{
	  if (nb == 0)
	    break;
	  mu_opool_append (pool, rbuf, nb);
	}
      else if (ret == EAGAIN)
	{
	  if (attempt < io_attempts)
	    {
	      ret = http_stream_wait (stream, MU_STREAM_READY_RD, &attempt);
	      if (ret)
		{
		  mu_error (_("http_stream_wait failed: %s"),
			    mu_strerror (ret));
		  return 1;
		}
	    }
	  else
	    {
	      mu_error ("mu_stream_read: %s", mu_strerror (ret));
	      return 1;
	    }
	}
    }

  ret = mu_stream_close (stream);
  if (ret != 0)
    mu_error ("mu_stream_close: %s", mu_strerror (ret));
  return ret;
}

static int
read_project_page (const char *project, mu_opool_t pool)
{
  int ret;
  char *host;
  char *path;
  char *cmd;
  unsigned port = 0;
  mu_stream_t stream;
  size_t attempt;
  
  if (create_project_url (project, &host, &path, &port))
    return 1;

  if (port == 0)
    port = 80;

  if (mu_asprintf (&cmd,
		   "GET /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n\r\n",
		   path,
		   host))
    mu_alloc_die ();

  if (verbose > 2)
    mu_printf (_("Connecting to %s:%u\n"), host, port);
  
  ret = mu_tcp_stream_create (&stream, host, port, MU_STREAM_NONBLOCK);
  for (attempt = 0; ret; )
    {
      if ((ret == EAGAIN || ret == EINPROGRESS) && attempt < io_attempts)
	{
	  ret = http_stream_wait (stream, MU_STREAM_READY_WR, &attempt);
	  if (ret == 0)
            {
              ret = mu_stream_open (stream);
              continue;
            }
	}
      mu_error ("mu_stream_open: %s", mu_strerror (ret));
      return 1;
    }

  if (ret == 0 && (ret = http_send (stream, cmd)) == 0)
    {
      if (verbose > 2)
	mu_printf (_("Getting /%s\n"), path);      
      ret = http_read (stream, pool);
    }

  mu_stream_destroy (&stream);
  free (host);
  free (path);
  free (cmd);
  
  return ret;
}

static int
http_ok (const char *text)
{
  int code;
  
  if (verbose > 2)
    mu_printf (_("Got %-12.12s\n"), text);
      
  /* A minimal response is 12 bytes long:
        HTTP/1.1 200
  */	
  if (strlen (text) < 12)
    {
      mu_error (_("Invalid HTTP response (got %s)"), text);
      return 1;
    }
  
  if (memcmp (text, "HTTP/", 5)
      || !mu_isdigit (text[5]) || text[6] != '.' || !mu_isdigit (text[7])
      || !mu_isblank (text[8]))
    {
      mu_error (_("Invalid HTTP response (got %*.*s)"), 8, 8, text);
      return 1;
    }
  for (text += 8; *text; text++)
    if (!mu_isblank (*text))
      break;

  if (*text == 0)
    {
      mu_error (_("Invalid HTTP response: not enough data"));
      return 1;
    }

  if (sscanf (text, "%3d", &code) != 1)
    {
      mu_error (_("Invalid HTTP response (near %s)"), text);
      return 1;
    }

  if (code == 404)
    {
      mu_error (_("Page not found"));
      return 1;
    }

  if (code != 200)
    {
      mu_error (_("Got error %d"), code);
      return 1;
    }
  
  return 0;
}
  
int
verify_pot_version (const char *project, const char *vers)
{
  int rc;
  mu_opool_t pool;

  if (!pot_regex)
    return 1;
  
  if (verbose > 1)
    mu_printf (_("Verifying POT file version for `%s'\n"), project);
  mu_opool_create (&pool, MU_OPOOL_ENOMEMABRT);
  rc = read_project_page (project, pool);
  if (rc == 0)
    {
      char *text;
      regmatch_t match[2];

      mu_opool_append_char (pool, 0);
      text = mu_opool_finish (pool, NULL);
      if (http_ok (text))
	exit (1);
      
      if (regexec (pot_regex, text, sizeof match / sizeof match[0], match, 0))
	{
	  mu_error (_("URL of the latest POT file not found"));
	  rc = 1;
	}
      else
	{
	  size_t len = match[1].rm_eo - match[1].rm_so;
	  char *package_str, *pot_str;

	  if (len > 4 && memcmp (text + match[1].rm_eo - 4, ".pot", 4) == 0)
	    len -= 4;
	  mu_opool_append (pool, text + match[1].rm_so, len);
	  mu_opool_append_char (pool, 0);
	  package_str = mu_opool_finish (pool, NULL);

	  mu_opool_appendz (pool, project);
	  mu_opool_append_char (pool, '-');
	  mu_opool_appendz (pool, vers);
	  mu_opool_append_char (pool, 0);
	  pot_str = mu_opool_finish (pool, NULL);

	  if (strcmp (package_str, pot_str))
	    {
	      mu_error (_("POT project string (%s) does not match latest version (%s)"),
			pot_str, package_str);
	      exit (1);
	    }
	}
    }
  mu_opool_destroy (&pool);
  return rc;
}

void
compile_pot_regex_str ()
{
  regex_t re;
  int rc = regcomp (&re, pot_regex_str, REG_EXTENDED);
  if (rc)
    {
      regerror(rc, &re, rbuf, sizeof rbuf);
      mu_error (_("Cannot compile regex: %s"), rbuf);
      exit(1);
    }
  if (re.re_nsub < 1)
    {
      mu_error (_("Invalid regex: no subexpressions"));
      exit(1);
    }
  pot_regex = mu_alloc (sizeof (pot_regex[0]));
  *pot_regex = re;
}
