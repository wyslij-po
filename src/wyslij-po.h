/* This file is part of Wyslij-po
   Copyright (C) 2007-2021 Sergey Poznyakoff

   Wyslij-po is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Wyslij-po is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Wyslij-po.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <mailutils/mailutils.h>
#include <regex.h>
#include <gettext.h>
#include <locale.h>

#define _(s) gettext(s)
#define N_(s) s

#define DEFAULT_LANGTAB_FILE SYSCONFDIR "/wyslij-po.lc"

#define VERIFY_PO_TIME         0x01
#define VERIFY_PACKAGE_VERSION 0x02

extern char *tp_url;
extern char *pot_regex_str;
extern int verify_mask;
extern int verbose;

char *restore_file_name (const char *, char **);
int parse_lang_datafile (const char *name);
int verify_pot_version (const char *project, const char *vers);
void compile_pot_regex_str (void);
     
